ARG ALPINE_VERSION
FROM alpine:${ALPINE_VERSION}
LABEL maintainer="https://gitlab.com/thiagocsf/varnish-alpine-docker"
ENV VARNISH_BACKEND_ADDRESS 192.168.1.65
ENV VARNISH_MEMORY 100M
ENV VARNISH_BACKEND_PORT 80
EXPOSE 80

RUN apk update && \
    apk upgrade && \
    apk add varnish

ADD start.sh /start.sh
CMD ["/start.sh"]
